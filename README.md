# python-pvprank

Rank Pokemon for the Great- and Ultra-League: Generate the IV-Rank table for a specified Pokemon (English, German or French names are supported) and export it to a CSV file. Interactively query specific IV combinations.

## Dependencies

The script runs in Python 3.6 or higher and requires
`pandas`, `numpy` and `matplotlib`.
(Tested with Python 3.6.6, pandas 0.23.4,
numpy 1.15.2, matplotlib 3.0.0)

This program uses Pokemon basestats from bulbapedia.net (
[https://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_base_stats_(Pok%C3%A9mon_GO)]),
CP Multipliers from gamepress.gg
[https://pokemongo.gamepress.gg/cp-multiplier]
and name translations from pokefans.net
[https://pokefans.net/pokedex/pokemon-liste/internationale-namen].


## Usage

Download or clone this repository, open a terminal,
navigate to the folder `python-pvprank` and run the
script `pvprank.py`.

```bash
python pvprank.py
```

Look at the quick help and the options with the `-h` flag:

```bash
python pvprank.py -h
```
```
usage: pvprank.py [-h] [-p] [-i] [-o] [-l {g,great,u,ultra}] [pokemon]

Generate the IV-Rank table for a Pokemon.

positional arguments:
  pokemon               Pokemon name (English or German)

optional arguments:
  -h, --help            show this help message and exit
  -p, --plot            make some plots
  -i, --interactive     popup dialog to select output folder
  -o, --open            open an existing table (skip the generation and jump
                        to plotting and querying the table.)
  -l {g,great,u,ultra}, --league {g,great,u,ultra}
                        select the league for ranking (default: great)
```

Other ways to run it:

```bash
# Generate the table for Bulbasaur
python pvprank.py Bulbasaur

# Generate the table for Bulbasaur, using the German name and small caps
python pvprank.py bisasam

# Show and export some silly plots
python pvprank.py --plot  # or '-p'

# Rank Pokemon for the Ultra-League
python pvprank.py -l ultra

# Select output folder interactively
python pvprank.py -i

# Combine it..
python pvprank -ip Tyranitar --league ultra
```

If the table for a specific Pokemon already exists on your harddrive,
you can run it with the `-o` flag to skip all the table-generating
procedures and jump straight to plotting or querying the table.
You will be prompted in a pop-up window to select an appropriate file:

```bash
# Open an existing table for plotting/querying..
python pvprank.py -o
```

In case of Pokemon with multiple forms, i.e., Alolans or the different
Giratinas, simply put the name without specifying the form.
The script will find the different forms in the basestat-data and
present the choices to the user.


## Installing Dependencies

### Ubuntu 18.04
Python 3.6 should already be installed with the system.
To install pandas (for python3), open a terminal and type:

```bash
sudo apt update
sudo apt install python3-pandas
```

(That should also install `numpy` and `matplotlib`, as they are
dependencies of `pandas`)

Download or clone this repository to your harddrive and you're good to go!

### Windows 10
Maybe use a Python distribution that already comes with `pandas`?
E.g., Anaconda..
