#!/usr/bin/env python3
"""Pokemon GO IV Ranker.

Requires Python 3.6+, pandas, numpy, matplotlib

Useful links:

Lists of pokemon basestats:
https://bulbapedia.bulbagarden.net/wiki/
List_of_Pok%C3%A9mon_by_base_stats_(Pok%C3%A9mon_GO)

Calculating the IV rankings:
https://pokemongo.gamepress.gg/analysis-ideal-iv-sets-pokemon-go-pvp
https://bulbapedia.bulbagarden.net/wiki/Statistic#In_Pok.C3.A9mon_GO

CP multiplier:
https://pokemongo.gamepress.gg/cp-multiplier

CP = floor((ATT * sqrt(DEF) * sqrt(HP) * CPmult**2) / 10.0)
if CP < 10: CP = 10

ZeChrales gamemaster data (not used in this script, sadly,):
https://github.com/ZeChrales/PogoAssets/blob/master/gamemaster/gamemaster.json

https://github.com/pokemongo-dev-contrib/pokemongo-json-pokedex
https://github.com/pokemongo-dev-contrib/pokemongo-game-master


TODO:
    * In general a problem with Alola naming schemes..
    * Make same rank for same stats_product (due to flooring STA)

"""
# import json
# import time
import argparse
import platform
from pathlib import Path
from datetime import datetime
import tkinter
from tkinter import Tk, filedialog

import numpy as np  # for sqrt()..
import pandas as pd
import matplotlib
from matplotlib import pyplot as plt

# GLOBALS
DIR_IN = Path("./data").resolve()
DIR_OUT = Path.home() / "pogo-pvprank"
RANKTABLE_FILETAG = "ranked"
RANKTABLE_PATTERN = "*_ranked_*.csv"
# if not DIR_OUT.is_dir():
#     DIR_OUT.mkdir(parents=True)

# Basestats
BASESTATS_TESMATH = Path("./data/stats_tesmath.csv").resolve()
BASESTATS_BULBAPEDIA = Path("./data/stats_bulbapedia.csv").resolve()
# CP-Multipliers
CP_MULTIPLIERS = Path("./data/cp_multipliers.csv")
# Translations
NAME_TRANSLATIONS = Path("./data/names.csv").resolve()
TIME_FMT = "%y%m%d-%H%M%S-utc"

# CP_MULT_LV40 = 0.79030001**2

GREAT_LEAGUE_CAP = 1500
ULTRA_LEAGUE_CAP = 2500

# Argument parsing
parser = argparse.ArgumentParser(
    description="Generate the IV-Rank table for a Pokemon.")
parser.add_argument("pokemon", nargs="?", default=None, type=str,
                    help="Pokemon name (English or German)")
# parser.add_argument("-d", "--debug", action="store_true",
#                     help="debug mode")
parser.add_argument("-p", "--plot", action="store_true",
                    help="make some plots")
parser.add_argument("-i", "--interactive", action="store_true",
                    help="popup dialog to select output folder")
parser.add_argument("-o", "--open", action="store_true",
                    help=("open an existing table (skip the "
                          "generation and jump to plotting"
                          "and querying the table.)"))
parser.add_argument("-l", "--league", default="great",
                    choices=["g", "great", "u", "ultra"],
                    help=("select the league for ranking " +
                          "(default: %(default)s)"))
# ARGS = vars(ap.parse_args())
ARGS = parser.parse_args()


def now_str(pattern=TIME_FMT):
    """Return a formatted timestring for the current time."""
    # return time.strftime(pattern, time.gmtime())
    return datetime.utcnow().strftime(pattern)


def initialize_io(
        ffn_base=BASESTATS_BULBAPEDIA,
        ffn_names=NAME_TRANSLATIONS,
        ffn_cp_mults=CP_MULTIPLIERS,
        dir_out=DIR_OUT,
        great_cap=GREAT_LEAGUE_CAP,
        ultra_cap=ULTRA_LEAGUE_CAP,
        args=ARGS,
):
    """Read input data and setup output folder."""
    print("\nPvP-Rank: Generate IV-Rank tables " +
          "for the Great- and Ultra-League.\n")

    print("Platform info: ")
    print(f"Python {platform.python_version()} "
          f"on {platform.uname()}")
    print(f"pandas version: {pd.__version__}")
    print(f"numpy version: {np.__version__}")
    print(f"matplotlib version: {matplotlib.__version__}")
    print(f"tkinter version: {tkinter.TkVersion}\n")
    # print("")

    # Read in basestats, CP multipliers and name translations
    base_df = pd.read_csv(ffn_base)
    cp_mults = pd.read_csv(ffn_cp_mults)
    names_df = pd.read_csv(ffn_names)  # , encoding='latin-1')

    # Determine CP cap
    if args.league.lower().startswith("g"):
        # Great League
        cap = great_cap
        args.league = "Great"
    elif args.league.lower().startswith("u"):
        # Ultra League
        cap = ultra_cap
        args.league = "Ultra"
    print(f"{args.league}-League: cap at {cap} CP")

    # Set up output folder
    if args.interactive:
        # Include this to make the crappy empty window go away
        root = Tk()
        root.withdraw()

        title = "Select output folder"
        dir_select = filedialog.askdirectory(parent=root, title=title,
                                             initialdir=dir_out)
        if not dir_select:
            print(f"Cancelled by user, falling back to using "
                  f"'{dir_out}' for output.")
        else:
            dir_out = Path(dir_select)

    if not dir_out.is_dir():
        dir_out.mkdir(parents=True)
        print(f"Created folder '{dir_out}'")

    return base_df, cp_mults, names_df, cap, dir_out


def load_ranktable(dir_out, rank_pattern=RANKTABLE_PATTERN, args=ARGS):
    """Load a ranktable."""

    # if args.pokemon is not None:
    #     # # Only look for the specified pokemon
    #     # rank_pattern = args.pokemon + rank_pattern
    #
    #     # Choose newest file from all available
    #

    # Include this to make the crappy empty window go away
    root = Tk()
    root.withdraw()
    # Set options
    opts = {}
    opts["parent"] = root
    opts["title"] = "Select a ranktable"
    opts["initialdir"] = dir_out
    opts['filetypes'] = [("PvP IV-RankTable", rank_pattern)]

    # Open table loop
    exit = False
    while not exit:
        # try:
        ffn_ranktable = filedialog.askopenfilename(**opts)
        # if ffn_ranktable is not None:
        # try:
        # Convert to pathlib.Path
        p_ranktable = Path(ffn_ranktable)
        # Read in CSV
        rank_df = pd.read_csv(p_ranktable)
        # Extract the Pokemon as the first part of the filename
        name = p_ranktable.stem.split("_")[0]
        exit = True
        # except ValueError as err:
        #     pass
    return rank_df, name


def get_name(pkmn, names_df):
    """Return English and German Pokemon name."""
    # Try English first
    if pkmn in list(names_df.English):
        row = names_df[names_df.English == pkmn]
    elif pkmn in list(names_df.German):
        row = names_df[names_df.German == pkmn]
    elif pkmn in list(names_df.French):
        row = names_df[names_df.French == pkmn]
    else:
        print(f"Couldn't find '{pkmn}' in the dictionary!")
        # row = None
        # Let it fail!
        return None

    # If name ocurs multiple times, pick the first one
    name_eng = row.English.values[0]
    name_ger = row.German.values[0]

    return name_eng, name_ger


def input_pkmn(names_df, query=None):
    """Select a valid Pokemon."""
    # User inputs a pokemon
    # name_eng = None  # Let it fail!
    exit = False
    while not exit:

        if query is None:
            query = input("Pokemon name: ")
            if (query == "exit") or (query == "quit"):
                print("\nCrashing the program now,)")
                exit = True
        # Capitalize first letter
        query = query.lower().capitalize()

        try:
            # Need english and german for stats and calcyiv..
            name_eng, name_ger = get_name(query, names_df)
            print(f"Found {name_eng}/{name_ger}")

            exit = True
        except TypeError as err:
            print(f"Wrong input '{query}', TypeError: {err}\n")
            query = None

    return name_eng  # , name_ger


def get_basestats(stats_df, name):
    """Retrieve the basestats for a given (English) Pokemon name."""
    row = stats_df[stats_df.Name == name]
    row.reset_index(drop=True, inplace=True)

    if row.shape[0] > 1:
        print(f"Found more than one Pokemon named {name}:\n"
              f"{row.loc[:, ['#', 'Name', 'ATT', 'DEF', 'STA']]}")

        exit = False
        while not exit:
            try:
                # Query user
                i = int(input(
                    f"Select Pokemon [0-{row.shape[0] - 1}]: "
                ))

                # Trim to row
                row = row.iloc[i]
                print(f"{row.at['Name']} selected.")

                exit = True

            except ValueError as err:
                print(f"Wrong input '{i}', ValueError: {err}\n")
    elif row.shape[0] == 1:
        row = row.iloc[0]
    else:
        print(f"{name} not found in basestat data! Returning None.")

        return None

    basestats = row[["ATT", "DEF", "STA"]].values  # , "CP"]]
    # print(f"Row: {row}, basestats: {basestats}")
    return basestats


def get_cp(base, iv, cpm):
    """Compute CP value.

    base ... list of basestats [att, def, sta]
    iv ..... list of iv [att, def, sta]
    cpm .... CP Multiplier
    """
    c_att = base[0] + iv[0]
    c_def = base[1] + iv[1]
    c_sta = base[2] + iv[2]

    # cp = np.floor(
    #         (cpm**2 *
    #          c_att * np.sqrt(c_def) * np.sqrt(c_sta)) /
    #         10.0
    # )

    cp = np.floor(
            (cpm**2 / 10.0) *
            c_att * np.sqrt(c_def) * np.sqrt(c_sta)
    )

    if cp < 10:
        cp = 10.0

    return cp  # , [c_att, c_def, c_sta]


def get_max_level(base_ads, iv_ads, cpms, cap=GREAT_LEAGUE_CAP):
    """Compute max level and also return the cp and the cpm."""
    cp = 0
    lvl = 0
    cpm = 0

    for row in cpms:
        # row = [lvl, cpm]
        cpm_new = row[1]
        cp_new = get_cp(base_ads, iv_ads, cpm_new)

        if cp_new > cap:
            break
        else:
            cp = cp_new
            cpm = cpm_new
            lvl = row[0]

    return lvl, cp, cpm


def get_league_stats(base_ads, iv_ads, cpms, cap=GREAT_LEAGUE_CAP):
    """Return a numpy row array with infos for the given basestats.

    header = [
            "IV_A/D/S", "Level", "CP",
            "Att", "Def", "Sta", "Stat_Product"
            "IV_A", "IV_D", "IV_S", "CPM",
    ]
    """
    # Get max level and CP
    lvl, cp, cpm = get_max_level(base_ads, iv_ads, cpms, cap=cap)

    # Calculate properties of the max_CP Pokemon
    c_att = (base_ads[0] + iv_ads[0]) * cpm
    c_def = (base_ads[1] + iv_ads[1]) * cpm
    c_sta = np.floor((base_ads[2] + iv_ads[2]) * cpm)

    iv_str = f"{iv_ads[0]}/{iv_ads[1]}/{iv_ads[2]}"

    statproduct = c_att * c_def * c_sta

    row = [
            iv_str, lvl, cp,
            c_att, c_def, c_sta, statproduct,
            iv_ads[0], iv_ads[1], iv_ads[2], cpm,
    ]

    return row


def make_plots(data, name, dir_out, args=ARGS):
    """Generate and store some plots."""
    resolution = (19.2, 10.8)
    title_fs = 26
    axis_fs = 22
    tick_fs = 18
    legend_fs = 18
    marker_sz = 3
    # Rank vs IV

    xx = data["Rank"].values
    fig, ax1 = plt.subplots(figsize=resolution, dpi=100)
    # ax2 = ax1.twinx()
    ax1.plot(xx, data["IV_A"].values, "r", lw=1, label="IV_ATT")
    ax1.plot(xx, data["IV_D"].values, "b", lw=1, label="IV_DEF")
    ax1.plot(xx, data["IV_S"].values, "g", lw=1, label="IV_STA")
    # ax2.plot(xx, data["Att"].values, "r--", lw=1, label="ATT")
    # ax2.plot(xx, data["Def"].values, "b--", lw=1, label="DEF")
    # ax2.plot(xx, data["Sta"].values, "g--", lw=1, label="STA")

    ax1.tick_params(labelsize=tick_fs)

    ax1.set_xlabel("Rank", fontsize=axis_fs)
    ax1.set_ylabel("IV", fontsize=axis_fs)
    # ax2.set_ylabel("Stats")
    ax1.set_xlim([1, 100])
    ax1.legend(
            # borderaxespad=0.,
            loc="upper right",  # bbox_to_anchor=(1.1, 1.0),
            fontsize=legend_fs,
            fancybox=True,
            framealpha=0.5,
    )  # loc=0)
    # ax2.legend(loc=0)
    plt.title(f"{name}: IV vs. Rank", fontsize=title_fs)
    plt.tight_layout()
    # Export the file
    filepath = (
            dir_out /
            f"{name}_rank-iv_{args.league.lower()}_{now_str()}.png"
    )
    plt.savefig(filepath)
    print(f"\nExported figure to {filepath}\n")
    plt.show()
    # plt.show(block=False)
    # plt.pause(0.1)
    # time.sleep(1)
    plt.close()
    # input("Press any key..")

    # CP and level plot
    fig, ax1 = plt.subplots(figsize=resolution, dpi=100)
    ax2 = ax1.twinx()
    ax1.plot(xx, data["CP"].values, "r", label="CP")
    ax2.plot(xx, data["Level"].values, "k*", ms=marker_sz, label="Level")
    ax1.tick_params(labelsize=tick_fs)
    ax2.tick_params(labelsize=tick_fs)
    ax1.set_xlabel("Rank", fontsize=axis_fs)
    ax1.set_ylabel("Combat Points (CP)", color="r", fontsize=axis_fs)
    ax2.set_ylabel("Level", color="k", fontsize=axis_fs)
    ax1.set_xlim(1, 4096)
    plt.title(f"{name}: CP and Level vs. Rank", fontsize=title_fs)
    plt.tight_layout()
    # Export the file
    filepath = (
            dir_out /
            f"{name}_rank-cp-lvl_{args.league.lower()}_{now_str()}.png"
    )
    plt.savefig(filepath)
    print(f"\nExported figure to {filepath}\n")
    plt.show()
    # plt.show(block=False)
    # fig.canvas.draw_idle()
    # fig.canvas.start_event_loop(0.1)

    # plt.pause(0.1)
    # input("Press any key..")
    # time.sleep(1)
    plt.close()
    return  # (fig, ax), (fig2, ax1, ax2)


def get_iv_input(name):
    """Parse in ."""
    query_iv = None

    exit = False
    # exit_program = False
    while not exit:
        iv_str = input(f"Rank {name} IVs [ATT DEF STA]: ")
        if (iv_str == "exit") or (iv_str == "quit"):
            print("\nCrashing the program now,)")
            exit = True
        else:
            try:
                i_att, i_def, i_sta = iv_str.split(" ")
                i_att, i_def, i_sta = int(i_att), int(i_def), int(i_sta)

                assert(0 <= i_att <= 15), "Wrong IV-Attack!"
                assert(0 <= i_def <= 15), "Wrong IV-Defense!"
                assert(0 <= i_sta <= 15), "Wrong IV-Stamina!"

                # Build iv_str
                query_iv = f"{i_att}/{i_def}/{i_sta}"
                exit = True

            except ValueError as err:
                print(f"Wrong input '{iv_str}', ValueError: {err}\n")

            except AssertionError as err:
                print(f"Wrong input '{iv_str}', AssertionError: {err}\n")

        # except KeyboardInterrupt:
        #     print("Leaving the program")

    return query_iv


def generate_ranktable(
        base_df, cpm_df, names_df, dir_out,
        cap=GREAT_LEAGUE_CAP,
        rank_tag=RANKTABLE_FILETAG,
        args=ARGS,
):
    """Rank Pokemon for PvP based on their IV."""

    # Select a valid Pokemon
    name_eng = input_pkmn(names_df, args.pokemon)

    # Retrieve basestats of the specified Pokemon
    base_ads = get_basestats(base_df, name_eng)
    print(f"BaseStats: {base_ads}")

    # Convert the CP Multiplier DataFrame into an array (for speed)
    cpm_ar = cpm_df.values

    # Iterate over all IV combinations..
    all_ivs = []
    for i in range(16):
        for j in range(16):
            for k in range(16):
                row = get_league_stats(base_ads, [i, j, k], cpm_ar, cap=cap)
                all_ivs.append(row)
                # print(row)

    # Convert data to pandas Dataframe
    header = [
            "IV_A/D/S", "Level", "CP",
            "Att", "Def", "Sta", "Stat_Product",
            "IV_A", "IV_D", "IV_S", "CPM",
    ]
    ranked = pd.DataFrame(all_ivs, columns=header)
    print(f"Ranked {name_eng}, shape: {ranked.shape}")
    # print(ranked)

    # Sort by Stat Product
    rank_sorted = ranked.sort_values("Stat_Product", ascending=False)
    rank_sorted.reset_index(drop=True, inplace=True)
    # print(rank_sorted)

    # Add columns for relative strengths
    sp = rank_sorted["Stat_Product"]
    max_sp = sp.max()
    min_sp = sp.min()
    sp_diff = max_sp - min_sp
    rank_sorted["Percent_Max"] = 100 * (
            sp / max_sp
    )
    rank_sorted["Percent_Rel"] = 100 * (
            (sp - min_sp) / sp_diff
    )

    # Add the Rank column and set it as the index
    rank_sorted["Rank"] = list(range(1, rank_sorted.shape[0] + 1))
    # rank_sorted.set_index("Rank", inplace=True)

    # Reorder columns
    rank_sorted = rank_sorted[[
            "Rank", "IV_A/D/S", "Level", "CP",
            "Att", "Def", "Sta",
            "Percent_Rel", "Percent_Max", "Stat_Product",
            "IV_A", "IV_D", "IV_S", "CPM",
    ]]

    # Export table
    outpath = (
            dir_out /
            f"{name_eng}_{rank_tag}_{args.league.lower()}_{now_str()}.csv"
    )
    rank_sorted.to_csv(outpath, index=False)
    print(f"\nExported CSV to {outpath}\n")

    return rank_sorted, name_eng


def main(rank_tag=RANKTABLE_FILETAG, args=ARGS):
    """Rank Pokemon for PvP based on their IV."""

    # Get input data
    base_df, cpm_df, names_df, cap, dir_out = initialize_io()
    print("Read in basestats, cpms, names, league-cap.")
    print(f"\nOutput to {dir_out}\n")

    # Get ranktable as pandas.DataFrame
    if args.open:
        # print("Loading ranktable")
        rank_df, name = load_ranktable(dir_out)
    else:
        rank_df, name = generate_ranktable(
                base_df, cpm_df, names_df, dir_out, cap=cap,
        )
    rank_df.set_index("Rank", inplace=True, drop=False)
    print(rank_df)
    print(rank_df.describe())

    # Make plots
    if args.plot:
        # fig1, fig2 = make_plots(rank_df, name, dir_out)
        q = input("\nShow plots? [Y/n]: ").lower()
        if not q == "n":
            make_plots(rank_df, name, dir_out)

    # Query-loop to interactively rank specific IVs
    my_qs = None
    try:
        while True:
            # Query specific IVs in [0, 15]
            print("")
            query_iv = get_iv_input(name)

            tmp = rank_df[rank_df["IV_A/D/S"] == query_iv]

            if my_qs is None:
                my_qs = tmp.copy()
                print(f"Initialized my_qs: {my_qs}")
            else:
                # Append row to datdaframe and sort by rank
                my_qs = my_qs.append(tmp)
                my_qs.sort_index(inplace=True)  # , ascending=False)
            # print(tmp)
            print(f"Rank: {tmp['Rank'].values[0]}, "
                  f"Level: {tmp['Level'].values[0]}, "
                  f"CP: {tmp['CP'].values[0]}, "
                  f"max%: {round(tmp['Percent_Max'].values[0], 2):.2f}%, "
                  f"rel%: {round(tmp['Percent_Rel'].values[0], 2):.2f}%"
                  )
            print("")
            # The index (i.e., the Rank) should be displayed..
            print(f"{name} {args.league}-League Ranking")
            print(my_qs.loc[:, ["IV_A/D/S", "Level", "CP",
                                "Att", "Def", "Sta",
                                "Percent_Max", "Percent_Rel"]])
    finally:
        print("Finally!")
        # Export the assembled table
        if my_qs is not None:
            outpath = (
                    dir_out /
                    f"My-{name}s_{rank_tag}_{args.league.lower()}"
                    f"_{now_str()}.csv"
            )
            my_qs.to_csv(outpath, index=False)
            print(f"\nExported CSV to {outpath}\n")
        print("Done!")


if __name__ == "__main__":
    main()
